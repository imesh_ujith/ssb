import pylibftdi as ftdi
import platform
from collections import OrderedDict
import ftd2xx as ftd


def get_ftdi_device_list():
    devices = []
    for device in ftdi.Driver().list_devices():
        vendor, product, serial = device
        devices.append('%s: %s: %s' % (device, product, serial))
    print(devices)


def get_device_information():
    info = OrderedDict
    d = ftdi.Driver()

    info['pylibftdi version'] = ftdi.__VERSION__

    try:
        info['libftdi version'] = d.libftdi_version()
        info['libftdi library name'] = d._load_library('libftdi')._name
    except ftdi.LibraryMissingError:
        info['libftdi library'] = "Missing"

    try:
        info['libusb version'] = d.libusb_version()
        info['libusb library name'] = d._load_library('libusb')._name
    except ftdi.LibraryMissingError:
        info['libusb library'] = "Missing"
    info['Python version'] = platform.python_version()
    info['OS platform'] = platform.platform()

    for key, value in info.items():
        print("{:22}: {}".format(key, value))


def open_ftdi_device():
    open_device = ftd.open(0)
    if open_device:
        print(open_device.getDeviceInfo())

        ftdi.Driver.list_devices()

    else:
        print('Can\'t open FT2232H device')



